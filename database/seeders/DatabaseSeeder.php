<?php

namespace Database\Seeders;

use App\Models\Assignment;
use App\Models\Course;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {   
        //Admin sign in codes
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@mail.com',
            'password' => Hash::make('admin'),
            'role' => 3,
            'email_verified_at' => '2021-05-12 13:41:49',

        ]);
    
        //Create git course 
        Course::create([
            'name' => 'Git',
            'image' => 'images/git.jpeg',
            'description' => 'Leer gebruik te maken van Git',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        
        ]);    

        //Create git assignments 

        Assignment::create([
            'course_id' => '1',
            'name' => 'Git essentials',
            'description' => 'Gitrry out',
            'youtube_link' => 'https://www.youtube.com/watch?v=YiCsHyBwfuM&list=PLavMXd9y3NVAhcBtoW8ErtfXihuod1T1J&index=1',
            'avatar' => 'images/git-logo.jpeg',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        ]);

         //Create laravel course 
         Course::create([
            'name' => 'Laravel',
            'image' => 'images/Laravel.jpeg',
            'description' => 'Leer gebruik te maken van Laravel',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        
        ]);    

        //Create laravel assignments 
        Assignment::create([
            'course_id' => '2',
            'name' => 'Laravel installeren',
            'description' => 'In deze les ga je leren hoe je laravel instaleert',
            'youtube_link' => 'https://www.youtube.com/watch?v=7PYSzMpCb9U&list=PLavMXd9y3NVDip6G3S5xzODw4U6I5iNdN&ab_channel=StephanHoeksemaStephanHoeksema',
            'avatar' => 'images/Laravel-install.jpeg',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        ]);
        
        //Create laravel assignments 
        Assignment::create([
            'course_id' => '2',
            'name' => 'Composer installeren',
            'description' => 'In deze les ga je leren hoe je composer instaleert',
            'youtube_link' => 'https://www.youtube.com/watch?v=YiCsHyBwfuM&list=PLavMXd9y3NVAhcBtoW8ErtfXihuod1T1J&index=1',
            'avatar' => 'images/Composer-install.jpeg',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        ]);
        
        //Create laravel assignments 
        Assignment::create([
            'course_id' => '2',
            'name' => 'PHP Artisan',
            'description' => 'PHP artisan commands',
            'youtube_link' => 'https://www.youtube.com/watch?v=V3FjbZOBoL0&list=PLavMXd9y3NVDip6G3S5xzODw4U6I5iNdN&index=4&ab_channel=StephanHoeksema',
            'avatar' => 'images/php-commands.png',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        ]);
        //Create node.js course 
        Course::create([
            'name' => 'Node.js',
            'image' => 'images/Node.js_logo.svg.png',
            'description' => 'Leer gebruik te maken van Node.js',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        
        ]); 
        //Create node.js assignments 
        Assignment::create([
            'course_id' => '3',
            'name' => 'Node.js Crashcourse',
            'description' => 'In deze les leer je de basis van Node.js',
            'youtube_link' => 'https://www.youtube.com/watch?v=fBNz5xF-Kx4&list=PLillGF-RfqbZ2ybcoD2OaabW2P7Ws8CWu&ab_channel=StephanHoeksemaStephanHoeksema',
            'avatar' => 'images/Node.png',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        ]);
        
        //Create node.js assignments 
        Assignment::create([
            'course_id' => '3',
            'name' => 'MySQL gebruiken met Node.js',
            'description' => ' in deze les leer je MySQL gebruiken met Node.js',
            'youtube_link' => 'https://www.youtube.com/watch?v=EN6Dx22cPRI&list=PLillGF-RfqbZ2ybcoD2OaabW2P7Ws8CWu&index=9&ab_channel=TraversyMediaTraversyMediaGeverifieerd',
            'avatar' => 'images/MySQL-node.png',
            'created_at' => '2021-06-03 19:28:52',
            'updated_at'=> NOW(),
        ]);

        User::create([
            'name' => 'mert kirez',
            'username' => 'mert',
            'email' => 'mert@mail.com',
            'password' => Hash::make('mert00910'),
            'role' => 1,
            'email_verified_at' => now(),

        ]);

        User::create([
            'name' => 'diamé morreau',
            'username' => 'diame',
            'email' => 'diame@mail.com',
            'password' => Hash::make('diame'),
            'role' => 1,
            'email_verified_at' => now(),

        ]);

        User::create([
            'name' => 'Maksim ',
            'username' => 'Maksim',
            'email' => 'Maksim@mail.com',
            'password' => Hash::make('maksim'),
            'role' => 1,
            'email_verified_at' => now(),

        ]);

        User::create([
            'name' => 'Carlijn  den Hartog',
            'username' => 'Carlijn',
            'email' => 'carlijn@mail.com',
            'password' => Hash::make('carlijn'),
            'role' => 1,
            'email_verified_at' => now(),

        ]);

        User::create([
            'name' => 'Roy meijer',
            'username' => 'Roy',
            'email' => 'roy@mail.com',
            'password' => Hash::make('roy'),
            'role' => 1,
            'email_verified_at' => now(),

        ]);

        User::create([
            'name' => 'Rudy Borgstede',
            'username' => 'Rudy',
            'email' => 'rudy@mail.com',
            'password' => Hash::make('rudy'),
            'role' => 2,
            'email_verified_at' => now(),

        ]);

        User::create([
            'name' => 'Stephan Hoeksema',
            'username' => 'Stephan',
            'email' => 'stephan@mail.com',
            'password' => Hash::make('stephan'),
            'role' => 2,
            'email_verified_at' => now(),

        ]);
    }
}