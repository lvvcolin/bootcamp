<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function show()
    {   
        /*Simple paginate is a method from laravel that allows you to create pagination.
        The only thing that you need to is to tell it how many results you want on a single page 
        */
        $users = User::simplePaginate(15);
        return view('admin.show', compact('users'));
    }

    public function create(User $user)
    {   
        User::create($this->validateUser($user));

        return back();
    }

    public function store(Request $request)
    {
        //
    }

    public function edit()
    {
        //
    }

    public function update(User $user)
    {
        $user->update($this->validateUser($user));

        return back();
    }

    public function destroy(User $user)
    {
        $user->delete();

        return back();
    }

    public function validateUser(User $user)
    {

        $attributes = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($user)],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user)],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role' => ['required', 'integer'],
        ]);

        $attributes['password'] = bcrypt($attributes['password']);

        return $attributes;
    }
}
