<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Models\Reaction;



class ReactionController extends Controller
{
    public function store(Request $request)
    {
        Reaction::updateOrCreate(['id' => $request->reaction_id], $this->validateReaction());
    }

    public function destroy(Course $course, Assignment $assignment, Reaction $reaction)
    {
        $reaction->delete();
    }

    public function index(Course $course, Assignment $assignment)
    {

        return $assignment->reactions;
    }

    protected function validateReaction()
    {
        return request()->validate([
            'message' => 'required',
            'assignment_id' => 'required',
            'user_id' => 'required',
        ]);
    }
}
