<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
class ContactController extends Controller
{
    public function contact() 
    {
        $users = User::all();
        return view('contact',compact('users'));
    }


    public function sendEmail(Request $request) {
        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'msg' => $request->msg
        ];

        
        
        Mail::to($request->emailsendto)->send(new ContactMail($details));
        return back()->with('message_sent', 'Je bericht is verzonden!');
    }
}
