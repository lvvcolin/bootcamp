<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'username',
        'avatar',
        'email',
        'git',
        'azure',
        'password',
        'role',

    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',

    ];
    // User has many reactions
    public function reactions()
    {
        return $this->hasMany(Reaction::class);
    }

    // User has many files $user->files;
    public function files()
    {
        return $this->hasMany(File::class);
    }

    // This is the pivot table connection with Assignment model
    public function assignments()
    {
        return $this->belongsToMany(Assignment::class)->withPivot('assignment_id', 'completed_at', 'submitted_at');
    }
    // This function checks if the assignments of an course are completed 
    public function CompletedAssignments($course)
    {
        return $this->belongsToMany(Assignment::class)->where('course_id', $course->id)->wherePivot('completed_at', '!=', null)->get();
    }
    // ????
    public function isAdmin()
    {
        return (boolean)$this->Admin;
    }
    // Through this function you can see the username in the url (if you use it in the web)
    public function getRouteKeyName()
    {
        return "username";
    }
    // This function shows the profile picture on the user profile
    public function getAvatarAttribute($value)
    {
        return $value ? asset('storage/' . $value) : 'https://www.gravatar.com/avatar/';
    }
    // -------------------------- ROLES --------------------------

    public function student()
    {
        return $this->role == 1;
    }
    public function teacher()
    {
        return $this->role == 2;
    }
    public function moderator()
    {
        return $this->role == 3;
    }
    // This function changes the name of the roles above from numbers to tet
    public function getRoleName()
    {
        if($this->student()){
            return "Student";
        }
        
        elseif ($this->teacher()){
            return "Teacher";
        }    
        
        elseif ($this->moderator()){
            return "Moderator";  
        }
        else{
            return "Account not confirmed";
        }        
    }
    
}
