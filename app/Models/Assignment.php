<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;

    protected $guarded = [];

    // an assignment belongs to a course 
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    // an assignment has many files
    public function files()
    {
        return $this->hasMany(File::class);
    }
    // an assignment has many pictures
    public function images()
    {
        return $this->hasMany(Image::class);
    }
    // an assignment has many reactions 
    public function reactions()
    {
        return $this->hasMany(Reaction::class)->orderByDesc('created_at');
    }

    // This is the pivot table connection with User model
    public function users()
    {
        return $this->belongsToMany(User::class, 'assignment_user', 'assignment_id', 'user_id')->withPivot(['completed_at', 'submitted_at']);
    }
    // this function gets the image of an assignment 
    // public function getImageAttribute($value)
    // {
    //     if ($this->created_at == '2021-06-03 19:28:52') {

    //         return asset($value);
    //     } else {
    //         return asset('storage/' . $value);
    //     }
    // }

    // this gets the avatar of assignment
    public function getAvatarAttribute($value)
    {
        if ($this->created_at == '2021-06-03 19:28:52') {

            return asset($value);
        } else {
            return asset('storage/' . $value);
        }
    }
}
