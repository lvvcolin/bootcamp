<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $guarded = [];
    // A course has many assignments 
    public function assignments()
    {
        return $this->hasMany(Assignment::class);
    }
    // Through this funciton you can go to the previous course
    public function PrevCourse()
    {
        return Course::find($this->id - 1);
    }
    // This function checks if you Completed the git cursus before it allows the students to work on the rest 
    public function GitCourseCompleted($user)
    {

        $allAssignmentsGitCourse = Course::first()->assignments;
        $completedAssignmentsByUser = $user->CompletedAssignments(Course::first());

        $diff = $allAssignmentsGitCourse->diff($completedAssignmentsByUser);

        if (count($diff) > 0 || count($allAssignmentsGitCourse) == 0) {
            return False;
        } else {
            return True;
        }
    }
    // This function gets the image that belongs to the course
    public function getImageAttribute($value)
    {
        if($this->created_at == '2021-06-03 19:28:52') {

            return asset($value);
        }
        else {
            return asset('storage/' . $value);
        }
    }
}
