<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Assignment;


class Reaction extends Model
{
  use HasFactory;

  protected $guarded = [];
  protected $with = ['user'];

  //  a reaction belongs to a assignment 
  public function assignment()
  {
    return $this->belongsTo(Assignment::class);
  }
    
  //  a reaction belongs to a user
  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
