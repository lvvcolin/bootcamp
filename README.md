## Over Bootcamp

Coding Highway is een netflix voor cursussen

Gemaakt door Team D3

## Requirements
* PHP
* Composer
* SQL Server 

## Run Bootcamp

### Setup
Maak een file genaamd ".env"
Zorg ervoor dat deze file niet .env.txt of een ander fileextensie heeft.

Run "php artisan key:generate"

Configureer de .env om te wijzen naar jouw SQL database

Run php artisan migrate om de database klaar te zetten

Run "composer install" om alle packages te downloaden en installeren

Als geen errors zijn voorkomen bij deze instructies ga naar de deel "Start Application"

### Start Application
Run "php artisan serve" om de applicatie op te starten

## Configureer Bootcamp
Edit the .env to your desired settings

## Handleiding
check de Handleiding voor gebruiksinformatie


