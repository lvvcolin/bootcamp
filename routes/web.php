<?php



use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Mail\NewUserWelcomemail;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//test

Route::get('/', function () {
    return view('home');
});

Auth::routes(['verify' => true]);

Route::middleware('auth')->middleware('verified')->group(function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //Profile
    Route::get('/profiles/{user}', [App\Http\Controllers\ProfilesController::class, 'show'])->name('profile');
    Route::get('/profiles/{user:username}/edit', [App\Http\Controllers\ProfilesController::class, 'edit'])->name('profile_edit');
    Route::patch('/profiles/{user:username}', [App\Http\Controllers\ProfilesController::class, 'update'])->name('profile_update');
    Route::patch('/profiles/password/{user:username}', [App\Http\Controllers\ProfilesController::class, 'updatePassword'])->name('profile_updatePassword');

    //courses
    Route::get('/course', [App\Http\Controllers\CourseController::class, 'index'])->name('course_index');
    Route::get('/course/create', [App\Http\Controllers\CourseController::class, 'create'])->name('course_create');
    Route::get('/course/{course}/delete', [App\Http\Controllers\CourseController::class, 'delete'])->name('delete_Course');


    //assignments
    Route::get('/course/{course}/assignments', [App\Http\Controllers\AssignmentController::class, 'index'])->name('course_show');
    Route::get('/course/{course}/assignments/create', [App\Http\Controllers\AssignmentController::class, 'create'])->name('create_assignments');
    Route::get('/course/{course}/assignments/{assignment}', [App\Http\Controllers\AssignmentController::class, 'show'])->name('show_assignments');
    Route::get('/course/{course}/assignments/{assignment}/delete', [App\Http\Controllers\AssignmentController::class, 'delete'])->name('delete_assignment');

    //reactions
    Route::get('/course/{course}/assignments/{assignment}/reactions', [App\Http\Controllers\ReactionController::class, 'index']);
    Route::post('/course/{course}/assignments/{assignment}/reactions/store', [App\Http\Controllers\ReactionController::class, 'store']);
    Route::put('/course/{course}/assignments/{assignment}/reactions/store', [App\Http\Controllers\ReactionController::class, 'store']);
    Route::delete('/course/{course}/assignments/{assignment}/reactions/{reaction}/delete', [App\Http\Controllers\ReactionController::class, 'destroy']);

    //contact
    Route::get('/contact', [App\Http\Controllers\ContactController::class, 'contact'])->name('contact');
    Route::post('/send-message', [App\Http\Controllers\ContactController::class, 'sendEmail'])->name('contact.send');

    //admin
    Route::patch('/admin/{user:username}/update', [App\Http\Controllers\AdminController::class, 'update'])->name('admin_update');
    Route::get('/admin', [App\Http\Controllers\AdminController::class, 'show'])->name('admin');
    Route::get('/admin/{user:username}', [App\Http\Controllers\AdminController::class, 'index'])->name('admin_index'); //not used yet
    Route::post('/admin/create', [App\Http\Controllers\AdminController::class, 'create'])->name('admin_create');
    Route::post('/admin/{user:username}', [App\Http\Controllers\AdminController::class, 'store']);
    Route::delete('/admin/{user:username}', [App\Http\Controllers\AdminController::class, 'destroy'])->name('admin_delete');

    Route::resources([
        'users' => App\Http\Controllers\AdminUserController::class
    ]);
});

Route::get('/course/{course}/assignments/{assignment}/startAssignment', [App\Http\Controllers\AssignmentController::class, 'startAssignment'])->name('startAssignment');

Route::get('/course/{course}/assignments/{assignment}/show_file', [App\Http\Controllers\FileController::class, 'index'])->name('show_file');

Route::get('/course/{course}/assignments/{assignment}/create_file', [App\Http\Controllers\FileController::class, 'store'])->name('create_file');
