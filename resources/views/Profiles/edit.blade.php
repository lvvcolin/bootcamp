@extends('layouts.app')

@section('content')

<div class="container">

    <a href="{{ url()->previous() }}" class="bg-yellow-300 text-white rounded py-2 px-4 mb-6 hover:bg-yellow-200 float-right">Terug</a>
    <br>

    <form method="POST" action="{{ route('profile_update', [$user])}}" enctype="multipart/form-data">

        @csrf

        @method('PATCH')

        <div class="my-6">

            <label for="name" class="block mb-2 uppercase font-bold text-xs text-gray-700 ">

                Naam

            </label>

            <input class="border border-gray-400 p-2 w-full" type="text" name="name" id="name"
                value="{{ $user->name}}" required>

            @error('name')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror

        </div>

        <div class="mb-6">

            <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">

                Gebruikersnaam

            </label>

            <input class="border border-gray-400 p-2 w-full" type="text" name="username" id="username"
                value="{{ $user->username}}" required>

            @error('username')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror

        </div>

        <div class="mb-6">

            <label for="avatar" class="block mb-2 uppercase font-bold text-xs text-gray-700">

                Profielfoto

            </label>

            <input class="border border-gray-400 p-2 w-full" type="file" accept=".jpg" max-width="1920" max-height="1080" name="avatar" id="avatar"
                value="{{ $user->avatar}}">

            @error('avatar')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror

        </div>

        <div class="mb-6">

            <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">

                E-mail

            </label>

            <input class="border border-gray-400 p-2 w-full" type="email" name="email" id="email"
                value="{{ $user->email}}" required>

            @error('email')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror

        </div>

        <!--Repository links-->
        <div class="flex space-x-2">

            <!--GIT-->
            <label for="gitlab" class="mb-2 flex-1 uppercase font-bold text-xs text-gray-700">

               Git gebruikersnaam

            </label>


            <!--AZURE DEVOPS-->
            <label for="gitlab" class="mb-2 flex-1 uppercase font-bold text-xs text-gray-700">

                Azure Devops projectnaam

            </label>
        </div>

        <div class="mb-6 flex space-x-2">

            <input class="border border-gray-400 p-2 flex-1" type="text" name="git" id="git" placeholder="Voeg hier je account link toe"
                   value="{{ $user->git}}">

            @error('git')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror



            <input class="border border-gray-400 p-2 flex-1" type="text" name="azure" id="azure" placeholder="Voeg hier je account link toe"
                   value="{{ $user->azure}}">

            @error('azure')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror
        </div>
        <div class="mb-6">

            <button type="submit" class="bg-yellow-300 text-white rounded py-2 px-4 hover:bg-yellow-200">

                Opslaan

            </button>

        </div>

    </form>


    <form method="POST" action="{{ route('profile_updatePassword', [$user])}}" enctype="multipart/form-data">

    @csrf

    @method('PATCH')

    <!--Passwords-->
        <div class="mb-6">

            <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">

                Wachtwoord

            </label>

            <input class="border border-gray-400 p-2 w-full" type="password" name="password" id="password"
                required>

            @error('password')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror

        </div>

        <div class="mb-6">

            <label for="password_confirmation" class="block mb-2 uppercase font-bold text-xs text-gray-700">

                Bevestig wachtwoord

            </label>

            <input class="border border-gray-400 p-2 w-full" type="password" name="password_confirmation"
                id="password_confirmation" required>

            @error('password_confirmation')

            <p class="text-red-500 text-xs mt-2">{{ $message}}</p>

            @enderror

        </div>

        <div class="mb-6">

            <button type="submit" class="bg-yellow-300 text-white rounded py-2 px-4 hover:bg-yellow-200">

                Wijzig wachtwoord

            </button>

        </div>
    </form>

</div>

@endsection
