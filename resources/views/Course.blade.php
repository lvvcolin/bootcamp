@extends('layouts.app')
<!--floating image css-->
<style>
    .floating {
        animation-name: floating;
        animation-duration: 6s;
        animation-iteration-count: infinite;
        animation-timing-function: ease-in-out;
    }

    @keyframes floating {
        0% {
            transform: translate(0, 0px);
        }

        50% {
            transform: translate(0, 20px);
        }

        100% {
            transform: translate(0, -0px);
        }
    }
</style>

@section('content')
{{--<div class="bg-white -mt-5">--}}
<div class="container">
    <div class="row">
        @if(Auth::User()->moderator() || Auth::User()->teacher())
        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <button type="button" class="bg-yellow-300 text-white rounded mt-2 py-3 px-4 hover:bg-yellow-200"
                data-toggle="modal" data-target="#myModal">Voeg cursus toe</button>
        </div>
        @endif
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="/course/create" enctype="multipart/form-data">
                    @csrf
                    @method('GET')
                    <div class="inner-form" style="padding: 39px;">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-l-12 col-xl-12 mr-12 inner-text">
                            <h1 class="text-xl-center font-semibold">Voeg cursus toe</h1>
                            <br>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Naam</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Beschrijving</label>
                            <input type="text" name="description" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Afbeelding</label>
                            <input type="file" name="image" class="form-control" id="image" aria-describedby="="
                                required>
                        </div>
                        <div class="form-group">
                            <button type="submit"
                                class="bg-yellow-300 text-white rounded mt-2 py-2 px-4 hover:bg-yellow-200">Voeg toe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- eerste --}}


{{-- tweede --}}
<div class="container">
    <div class="flex justify-center">
        <div class="text-center tracking-wider text-3xl text-gray-800 border-b-2 border-yellow-200">
            Cursussen
        </div>
    </div>
    <div class="row">
        @if(Auth::User()->student())
        <div class="grid grid-cols-3 gap-8 m-16">
            <!-- @foreach($courses as $course)
            @if ($loop->first)
            <div
                class="p-2 bg-gray-100 border-8 border-gray-100 rounded shadow-md hover:border-gray-200 hover:bg-gray-200 hover:shadow-lg ">
                <a href="{{route('course_show',[$course->id])}}">
                    <img src="{{$course->image}}" alt="" class="rounded-lg rounded-lg h-48 w-full">
                </a>
                <h1 class="tracking-wide m-2">
                    {{$course->name}}
                </h1>
                <p class="font-light tracking-wider m-2 truncate">
                    {{$course->description}}
                </p>
            </div>


            @elseif($course->GitCourseCompleted(Auth()->user()))

            <div
                class="p-2 bg-gray-100 border-8 border-gray-100 rounded shadow-md hover:border-gray-200 hover:bg-gray-200 hover:shadow-lg ">
                <a href="{{route('course_show',[$course->id])}}">
                    <img src="{{$course->image}}" alt="" class="rounded-lg rounded-lg h-48 w-full">
                </a>
                <h1 class="tracking-wide m-2">
                    {{$course->name}}
                </h1>
                <p class="font-light tracking-wider m-2 truncate">
                    {{$course->description}}
                </p>
            </div>
            @else
            <div
                class="p-2 bg-gray-100 border-8 border-gray-100 rounded shadow-md hover:border-gray-200 hover:bg-gray-200 hover:shadow-lg ">
                    <img src="{{$course->image}}" alt="" class="rounded-lg rounded-lg h-48 w-full opacity-50">
                <h1 class="tracking-wide m-2">
                    {{$course->name}}
                </h1>
                <p class="font-light tracking-wider m-2 truncate">
                    {{$course->description}}
                </p>
            </div>
        </div>
    </div>

    @endif
    @endforeach -->

        @foreach($courses as $course)
        <div
            class="p-2 bg-gray-100 border-8 border-gray-100 rounded shadow-md hover:border-gray-200 hover:bg-gray-200 hover:shadow-lg ">
            <a href="{{route('course_show',[$course->id])}}">
                <img src="{{$course->image}}" alt="" class="rounded-lg rounded-lg h-48 w-full">
            </a>
            <h1 class="tracking-wide m-2">
                {{$course->name}}
            </h1>
            <p class="font-light tracking-wider m-2 truncate">
                {{$course->description}}
            </p>
            
        </div>
        @endforeach
    
    @endif
    @if(Auth::User()->moderator() || Auth::User()->teacher())
    <div class="grid grid-cols-3 gap-8 m-16">
        @foreach($courses as $course)
        <div
            class="p-2 bg-gray-100 border-8 border-gray-100 rounded shadow-md hover:border-gray-200 hover:bg-gray-200 hover:shadow-lg ">
            <a href="{{route('course_show',[$course->id])}}">
                <img src="{{$course->image}}" alt="" class="rounded-lg rounded-lg h-48 w-full">
            </a>
            <h1 class="tracking-wide m-2">
                {{$course->name}}
            </h1>
            <p class="font-light tracking-wider m-2 truncate">
                {{$course->description}}
            </p>
            <div class="mt-4">
                <a class="bg-yellow-300 text-white rounded py-2 px-4  hover:bg-yellow-200" onclick="return confirm('Are you sure you want to delete this?')" href="{{route('delete_Course',[$course->id] )}}">Verwijderen</a>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>

@if (count($courses) === 0)
<!--Hero-->
<div>
    <div class="container mt-20 mb-28 px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
        <!--Left Col-->
        <div class="flex flex-col w-full md:w-2/5 justify-center items-start text-center md:text-right">
            <p class="uppercase tracking-loose w-full">Oh nee!</p>
            <h1 class="my-4 text-4xl font-bold leading-tight">
                Er zijn op dit moment geen cursussen beschikbaar..
            </h1>
            <p class="leading-normal text-2xl ml-36 mb-8">
                Probeer het later nog een keer
            </p>
        </div>
        <!--Right Col-->
        <div class="floating w-full md:w-3/5 pl-8 py-6 text-center">
            <img class="w-full md:w-4/5 rounded-xl shadow-xl hover:shadow-2xl"
                src="https://i.pinimg.com/originals/57/1e/9c/571e9c7cbf95597f10526a41dd300510.gif" />
        </div>
    </div>
</div>
@endif
</div>
@endsection
