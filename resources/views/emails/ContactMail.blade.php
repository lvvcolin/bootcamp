<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vragen van studenten</title>
</head>
<body>
<div class="max-w-md py-4 px-8 bg-white shadow-lg rounded-lg my-20">
    <div>
        <h2 class="text-gray-800 text-3xl font-semibold">Iemand heeft je hulp nodig!</h2>
        <p class="mt-2 text-gray-600">Het probleem of de vraag: <br> {{$details['msg']}}</p>
    </div>
    <div class="flex justify-end mt-4">
        <p class="text-xl font-medium text-yellow-500">Groetjes! <br> {{$details['name']}} | {{$details['email']}} </p>
    </div>
</div>
</body>
</html>
