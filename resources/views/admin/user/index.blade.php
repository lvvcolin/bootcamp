@extends('layouts.app')

@section('content')
@if(Auth::User()->moderator())
<div class="container text-center">
    <div class="bg-yellow-200 w-full rounded border-0 p-3 mb-4">
        <p class="text-gray-800 text-lg tracking-wider">
            Goedendag <br>{{Auth::user()->name}}
        </p>
    </div>
    <!--SEARCH-->
    <div id="search">
        <input type="search" id="myInput" class="bg-yellow-white w-full rounded border-0 p-3 mb-4" placeholder="Zoeken...">
    </div>
    <!--USERS-->
    <table class="table rounded-sm bg-gray-100 mt-2">
        <thead>
        <tr>
            <th class="pl-4 tracking-wider font-normal" scope="col">Naam</th>
            <th class="tracking-wider font-normal" scope="col">Gebruikersnaam</th>
            <th class="tracking-wider font-normal" scope="col">Rol</th>
            <th class="tracking-wider font-normal" scope="col">Lid sinds</th>
        </tr>
        </thead>
        <tbody id="myTable">
        @foreach ($users as $user)
            <tr>
                <th class="pl-3 font-light" scope="row">{{ $user->name }}</th>
                <th class="pl-3 font-light" scope="row">{{ $user->username }}</th>
                <th class="pl-3 font-light" scope="row">{{ $user->getRoleName() }}</th>
                <th class="font-light" scope="row">{{ date('d/m/Y', strtotime($user->created_at)) }}</th>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
@else
<h1>je bent geen admin</h1>
@endif
@endsection
