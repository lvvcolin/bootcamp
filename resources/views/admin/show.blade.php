@extends('layouts.app')
@section('content')
@if(Auth::User()->moderator())
<div class="container">
    <!--BUTTON-->
    <div class="row mb-6">
        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <button type="button" class="bg-yellow-300 text-white rounded mt-2 mr-3 py-3 px-4 hover:bg-yellow-200"
                data-toggle="modal" data-target="#myModall">Voeg gebruiker toe</button>
        </div>
    </div>


    <div class="container px-3 mx-auto flex flex-wrap flex-col md:flex-row ">
        <!--Left Col-->
        <div class="flex flex-col w-2/12 justify-center items-start text-center md:text-left">
            <nav
                class="flex flex-col w-full bg-yellow-300 h-full px-4 tex-gray-900 border rounded-md border-yellow-100 ">
                <div
                    class="flex flex-wrap mt-8 ml-2 text-white tracking-wider text-center text-lg border-b-4 border-white p-6">
                    Welkom <br> {{ Auth::user()->name }}!
                </div>
{{--                <div class="mt-10 mb-4">--}}
{{--                    <ul class="ml-4">--}}
{{--                        <li--}}
{{--                            class="mb-2 px-4 py-4 text-gray-100 tracking-wider font-light text-base flex flex-row  border-gray-300 hover:text-black   hover:bg-yellow-100  hover:font-bold rounded rounded-lg">--}}
{{--                            <a href="#">--}}
{{--                                <span>Dashboard</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li--}}
{{--                            class="mb-2 px-4 py-4 text-gray-100 tracking-wider font-light text-base flex flex-row  border-gray-300 hover:text-black   hover:bg-yellow-100  hover:font-bold rounded rounded-lg">--}}
{{--                            <a href="#">--}}

{{--                                <span>#</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li--}}
{{--                            class="mb-2 px-4 py-4 text-gray-100 tracking-wider font-light text-base flex flex-row  border-gray-300 hover:text-black   hover:bg-yellow-100  hover:font-bold rounded rounded-lg">--}}
{{--                            <a href="#">--}}
{{--                                <span>#</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
            </nav>
        </div>
        <!--Right Col-->
        <div class="w-10/12 pl-3 text-center">
            <!--SEARCH-->
            <div id="search">
                <input type="search" id="myInput" class="bg-yellow-white w-full rounded border-0 p-3 mb-4"
                    placeholder="Zoeken...">
            </div>
            <!--USERS-->
            <table class="table rounded-sm bg-gray-100 mt-2">
                <thead>
                    <tr>
                        <th class="pl-4 tracking-wider font-normal" scope="col">Naam</th>
                        <th class="tracking-wider font-normal" scope="col">Gebruikersnaam</th>
                        <th class="tracking-wider font-normal" scope="col">Rol</th>
                        <th class="tracking-wider font-normal" scope="col">Lid sinds</th>
                        <th class="tracking-wider font-normal" scope="col">Acties</th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    @foreach ($users as $user)
                    <tr>
                        <th class="pl-3 font-light" scope="row">{{ $user->name }}</th>
                        <th class="pl-3 font-light" scope="row">{{ $user->username }}</th>
                        <th class="pl-3 font-light" scope="row">{{ $user->getRoleName() }}</th>
                        <th class="font-light" scope="row">{{ date('d/m/Y', strtotime($user->created_at)) }}</th>
                        <td class="flex">
                            <a title="Show profile" id="edit" href="{{ route('profile', $user->username) }}">
                                <ion-icon name="open-outline"></ion-icon>
                            </a>
                            <form method="POST" action="{{ route('admin_delete', $user->username) }}"> @csrf
                                @method('DELETE')
                                <!-- <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this?')" data-toggle="confirmation">Remove</button> -->
                                <button title="Remove" id="remove" type="submit"
                                    onclick="return confirm('Are you sure you want to delete this?')"
                                    data-toggle="confirmation"
                                    style="	background: none; border: none; padding: 0; outline: inherit;">
                                    <ion-icon name="close-outline"></ion-icon>
                                </button>
                            </form>
                            <button title="Edit" id="edit" data-toggle="modal"
                                data-target="#userUpdateModal-{{ $user->id }}"
                                style="background: none; border: none; padding: 0; outline: inherit;">
                                <ion-icon name="create-outline"></ion-icon>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    {{-- The links method renders the links to the rest of the pages in the result set. --}}
                    {{ $users->links() }}
                </tbody>
            </table>
        </div>
        <!--container-->
    </div>

    <!--ADD USER-->
    <div id="myModall" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="/admin/create" enctype="multipart/form-data">
                    @csrf
                    <div class="inner-form" style="padding: 30px">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-l-12 col-xl-12 inner-text">
                            <h1 class="text-xl-center font-semibold">Voeg gebruiker toe</h1>
                            <br>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Naam</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Gebruikersnaam</label>
                            <input type="text" name="username" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" required>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="=" required>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Wachtwoord</label>
                            <input type="password" name="password" class="form-control" id="exampleInputEmail1"
                                aria-describedby="=" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Bevestig wachtwoord</label>
                            <input type="password" name="password_confirmation" class="form-control"
                                id="exampleInputEmail1" aria-describedby="=" required>
                        </div>


                        <div class="input-group mb-3    ">
                            <label class="input-group-text" for="inputGroupSelect01">Rol</label>
                            <select class="form-select" id="inputGroupSelect01" type="integer" name="role" required>
                                <option selected>Kies...</option>
                                <option value="1">Student</option>
                                <option value="2">Docent</option>
                                <option value="3">Admin</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit"
                                class="bg-yellow-300 text-white rounded mt-2 mr-3 py-3 px-4 hover:bg-yellow-200">Voeg
                                toe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--UPDATE USER-->
    @foreach ($users as $user)
    <div id="userUpdateModal-{{ $user->id }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="{{ route('admin_update', $user) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    {{-- <input hidden type="text" name="azure" value="{{ $user->azure }}">
                    <input hidden type="text" name="github" value="{{ $user->github }}">
                    <input hidden type="text" name="gitlab" value="{{ $user->gitlab }}">
                    <input hidden type="text" name="avatar" value="{{ $user->avatar }}"> --}}

                    <div class="inner-form" style="padding: 30px">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-l-12 col-xl-12 inner-text">
                            <h1 class="text-xl-center font-semibold">Pas gebruiker aan</h1>
                            <br>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Naam</label>
                            <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                value="{{ $user->name }}" aria-describedby="emailHelp" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Gebruikersnaam</label>
                            <input type="text" name="username" class="form-control" id="exampleInputEmail1"
                                value="{{ $user->username }}" aria-describedby="emailHelp" required>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                                value="{{ $user->email }}" aria-describedby="=" required>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Wachtwoord</label>
                            <input type="password" name="password" class="form-control" id="exampleInputEmail1"
                                aria-describedby="=" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Bevestig wachtwoord</label>
                            <input type="password" name="password_confirmation" class="form-control"
                                id="exampleInputEmail1" aria-describedby="=" required>
                        </div>


                        <div class="input-group mb-3    ">
                            <label class="input-group-text" for="inputGroupSelect01">Rol</label>
                            <select class="form-select" id="inputGroupSelect01" type="integer" name="role" required>
                                <option value="1" {{  $user->role == 1 ? 'selected' : '' }}>Student</option>
                                <option value="2" {{  $user->role == 2 ? 'selected' : '' }}>Docent</option>
                                <option value="3" {{  $user->role == 3 ? 'selected' : '' }}>Admin</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit"
                                class="bg-yellow-300 text-white rounded mt-2 mr-3 py-3 px-4 hover:bg-yellow-200">Pas aan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach


    </body>
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
</div>

@else

<h1>je bent geen admin</h1>
@endif
@endsection
