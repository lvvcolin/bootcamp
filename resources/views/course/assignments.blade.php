@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        @if(Auth::User()->moderator() || Auth::User()->teacher())
            <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                <button type="button" class="bg-yellow-300 text-white rounded mt-2 py-3 px-4 hover:bg-yellow-200" data-toggle="modal" data-target="#myModal">Voeg opdracht toe</button>
            </div>
        @endif
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form method="POST" action="{{route('create_assignments',[$course->id])}}" enctype="multipart/form-data">
                    @csrf
                     @method('GET')
                    <div class="inner-form" style="padding: 30px">
                        <input type="hidden" value="{{$course->id}}" name="course_id">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-l-12 col-xl-12 inner-text">
                            <h1 class="text-xl-center font-semibold">Voeg opdracht toe</h1>
                            <br>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Naam</label>
                            <input type="text"  name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Beschrijving</label>
                            <input type="text"  name="description" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Youtube link</label>
                            <input type="text"  name="youtube_link" class="form-control" placeholder="enter a url" id="image" aria-describedby="=" >
                            <div id="emailHelp" class="form-text font-light">Paste a youtube url instead of uploading a video</div>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Thumbnail</label>
                            <input type="file"  name="avatar" class="form-control" placeholder="enter a url" id="image" aria-describedby="=" required>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="bg-yellow-300 text-white rounded mt-2 py-2 px-4 hover:bg-yellow-200">Voeg toe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="flex justify-center">
        <div class="text-center tracking-wider text-3xl text-gray-800 border-b-2 border-yellow-200">
            Opdrachten
        </div>
    </div>
    <div class="grid grid-cols-3 gap-8 m-16">
        @foreach($course->assignments as $assignment)
            <div class="p-2 bg-gray-100 border-8 border-gray-100 rounded shadow-md hover:border-gray-200 hover:bg-gray-200 hover:shadow-lg ">
                <a href="{{url('/course/'. $course->id . '/assignments/' . $assignment->id )}}">
                    <img src="{{ $assignment->avatar }}" alt="" class="rounded-lg h-48 w-full">
                </a>
                <h1 class="tracking-wide m-2">
                    {{$assignment->name}}
                </h1>
                <p class="font-light tracking-wider m-2 truncate">
                    {{$assignment->description}}
                </p>

                @if(Auth::user()->teacher() || Auth::user()->moderator())
                    <div class="mt-4">
                        <a class="bg-yellow-300 text-white rounded py-2 px-4 hover:bg-yellow-200" href="{{route('delete_assignment',[$assignment->id,$course->id] )}}">Verwijderen</a>
                    </div>
                @else

                @endif
            </div>
        @endforeach
    </div>

</div>
@endsection
