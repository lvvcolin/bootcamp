@extends('layouts.app')
@section('content')
<body>
<section>
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 offset-md-3">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header text-center">--}}
{{--                        <p class="tracking-wider ">Ergens tegenaan gelopen tijdens de Bootcamp? <br>--}}
{{--                            Gebruik deze pagina om je vraag/probleem te stellen aan de docent</p>--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        @if(Session::has('message_sent'))--}}
{{--                            <div class="alert alert-succes" role="alert">--}}
{{--                                {{Session::get('message_sent')}}--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        <form method="POST" action="{{route('contact.send')}}" enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="name">Naam</label>--}}
{{--                                <input type="text" name="name" value="{{ auth()->user()->name}}" class="form-control">--}}
{{--                            </div>--}}

{{--                            <div class="form-group">--}}
{{--                                <label for="email">Email</label>--}}
{{--                                <input type="text" name="email" value="{{ auth()->user()->email}}"class="form-control">--}}
{{--                            </div>--}}

{{--                            <label for="msg">Kies docent</label>--}}
{{--                            <select  class="form-control" name="emailsendto">--}}
{{--                            @foreach($users->where('role',2) as $user)--}}
{{--                              <option value="{{$user->email}}" >{{$user->email}}</option>--}}
{{--                            @endforeach--}}
{{--                            </select>--}}

{{--                            <div class="form-group mt-2">--}}
{{--                                <label for="msg">Vraag en/of probleem</label>--}}
{{--                                <textarea name="msg" class="form-control"></textarea>--}}
{{--                            </div>--}}
{{--                            <button type="submit" class="btn btn-primary float-right">Verzenden</button>--}}
{{--                        </form>--}}
{{--                        <form method="get" action="http://127.0.0.1:8000/dashboard">--}}
{{--                            <button type="submit" class="float-right btn btn-secondary">back to home Page</button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="mt-10 sm:mt-0 flex justify-center">
        <div class="md:grid md:grid-cols-2 md:gap-6 w-50">
            <div class="mt-5 md:mt-0 md:col-span-2">
                <form method="POST" action="{{route('contact.send')}}" enctype="multipart/form-data">
                @csrf
                    <div class="shadow overflow-hidden sm:rounded-md">
                        <div class="px-4 py-3 bg-gray-50 text-center sm:px-6">
                            <p class=" py-2 px-4 text-lg font-light leading-loose">
                                Ergens tegenaan gelopen tijdens de Bootcamp? <br>
                                Stuur je docent jouw vraag en/of probleem
                            </p>
                            @if(Session::has('message_sent'))
                                <div class="bg-green-400 text-white p-2 rounded-md" role="alert">
                                    {{Session::get('message_sent')}}
                                </div>
                            @endif
                        </div>
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <div class="grid grid-cols-6 gap-6">
                                <div class="col-span-6 sm:col-span-6">
                                    <label for="first_name" class="block text-sm font-medium text-gray-700">Naam</label>
                                    <input type="text" name="name" value="{{ auth()->user()->name}}" class="mt-1 p-3 focus:ring-yellow-300 focus:border-yellow-300 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-6">
                                    <label for="last_name" class="block text-sm font-medium text-gray-700">E-mailadress</label>
                                    <input type="text" name="email" value="{{ auth()->user()->email}}" class="mt-1 p-3 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-6">
                                    <label class="block text-sm font-medium text-gray-700">Docent</label>
                                    <select name="emailsendto" class="mt-1 block w-full py-3 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                        @foreach($users->where('role',2) as $user)
                                            <option value="{{$user->email}}" >{{$user->email}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-span-6 sm:col-span-6">
                                    <label class="block text-sm font-medium text-gray-700">Bericht</label>
                                    <textarea rows="5" type="text" name="msg" class="mt-1 p-2 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-yellow-300 hover:bg-yellow-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-yellow-300">
                                Verstuur
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection



