<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Coding_Highway') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- embed styles zorgt ervoor dat wij youtube linkjes vanuit de url werkt -->
    <x-embed-styles />
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">


</head>
<body>
<div id="app">
    <nav class="bg-white shadow-md" role="navigation">
        <img src="{{ asset('../../../images/nav-logo.png') }}" alt="logo" class="h-14 w-14 float-left ml-12 mt-2">
        <div class="mx-auto  flex flex-wrap items-center md:flex-no-wrap">
        <div class="container mx-auto  flex flex-wrap items-center md:flex-no-wrap">

            <div class="w-full md:w-auto md:flex-grow md:flex md:items-center">
                <ul class="flex flex-col -mx-4 md:flex-row md:items-center md:mx-0 md:ml-auto md:mt-0 md:pt-0 md:border-0">

                    @guest
                        @if (Route::has('login'))
                            <li class="justify-end border-b-2 border-transparent hover:text-gray-800 hover:border-yellow-400 py-3 mx-1.5 sm:mx-6">
                                <a class="nav-link flex" href="{{ route('login') }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-door-open" viewBox="0 0 16 16">
                                        <path d="M8.5 10c-.276 0-.5-.448-.5-1s.224-1 .5-1 .5.448.5 1-.224 1-.5 1z"/>
                                        <path d="M10.828.122A.5.5 0 0 1 11 .5V1h.5A1.5 1.5 0 0 1 13 2.5V15h1.5a.5.5 0 0 1 0 1h-13a.5.5 0 0 1 0-1H3V1.5a.5.5 0 0 1 .43-.495l7-1a.5.5 0 0 1 .398.117zM11.5 2H11v13h1V2.5a.5.5 0 0 0-.5-.5zM4 1.934V15h6V1.077l-6 .857z"/>
                                    </svg>
                                    <span class="pl-1">Login</span>
                                </a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="justify-end border-b-2 border-transparent hover:text-gray-800 hover:border-yellow-400 py-3 mx-1.5 sm:mx-6">
                                <a class="nav-link flex" href="{{ route('register') }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
                                        <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                                    </svg>
                                    <span class="pl-1">Registreer</span>
                                </a>
                            </li>
                        @endif
                </ul>
                    @else

            <div class="container mx-auto pb-4 flex flex-wrap items-center md:flex-no-wrap">
                <div class="w-full md:w-auto md:flex-grow md:flex md:items-center">
                <!--LEFT-->
                <ul class="flex flex-col tracking-wider mt-2 -mx-4 pt-4 border-t md:flex-row md:items-center md:mx-0 md:mt-0 md:pt-0 md:mr-4 lg:mr-8 md:border-0">
                    <li>
                        <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 mx-1.5 sm:mx-6" href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 md:p-2" href="{{ route('profile', auth()->user()) }}">Profiel</a>
                    </li>
                    <li>
                        <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 mx-1.5 sm:mx-6" href="{{ route('course_index') }}">Cursussen</a>
                    </li>

                    @if (auth()->user()->student())
                    <li>
                        <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 md:p-2" href="{{ route('contact') }}">Contact</a>
                    </li>
                     @endif

                    @if (auth()->user()->teacher())
                    <li>
                        <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 md:p-2" href="{{ route('users.index') }}">Gebruikers</a>
                    </li>
                     @endif

                    @if (auth()->user()->moderator())
                        <li>
                            <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 md:p-2" href="{{ route('admin') }}">Admin panel</a>
                        </li>
                    @endif
                </ul>

                <!--RIGHT-->
                <ul class="flex flex-col mt-2 -mx-4 pt-4 border-t md:flex-row md:items-center md:mx-0 md:ml-auto md:mt-0 md:pt-0 md:border-0">
                    <li>
                        <a class="block text-gray-800 border-b-2 border-yellow-300 px-4 py-1 md:p-2 lg:px-4" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
                @endguest
            </div>
        </div>
    </div>
   </div>
</nav>
    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>

</html>
