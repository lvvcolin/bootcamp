@extends('layouts.app')

@section('content')

    <div class="mx-20 my-4">
        <a href="{{ url()->previous() }}" class="bg-yellow-300 text-white rounded py-2.5 px-4 mb-6 hover:bg-yellow-200">Terug</a>
    </div>
    <div class="container mt-16">
        <div class="grid grid-row-2">
            <div class="grid grid-cols-2 gap-4">
                <div class="h-25">
                    <x-embed url="{{$assignment->youtube_link}}" />
                </div>
                <div class="grid grid-row-2">
                    <div class="text-center text-2xl mb-4 tracking-wider">
                        {{$assignment->name}}
                    </div>
                    <div class="overflow-y-auto h-3/5 border-4 p-4">
                        {{$assignment->description}}
                    </div>
                </div>
            </div>
        </div>
        <div class="grid grid-row-2 mt-12">
            <div class="flex justify-center text-center text-2xl tracking-wider mb-6">
                <p class="border-b-2 border-yellow-300 w-25">Laat hier je reactie achter</p>
            </div>
            <div class="flex justify-center">
                <reactions :auth_user="{{ auth()->user() }}" :assignment="{{ $assignment }}"
                           class="w-75 text-center"></reactions>
            </div>
        </div>
    </div>

@endsection
